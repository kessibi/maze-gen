#ifndef __MAZE_H
#define __MAZE_H

#include <stdint.h>
#include <stdio.h>

struct node {
  unsigned int h, w, id;  // height, width, identification for pathfinding
  struct node *left, *right;
  int way, door[2];  // x, y in this order
  int8_t cut;        // cut: 0 horizontal, 1 vertical
};

typedef struct _maze maze;

/* cry: perror and exit failure */
void cry(char *str);

/* tree functions */
struct node *init_tree(int w, int h);
void free_tree(struct node *n);
unsigned int random_tree(struct node *n, int id);
void print_tree(struct node *n, int offset);

struct node *random_point(struct node *n);
struct node *common_node(struct node *n, struct node *a, struct node *b);
void maze_path(struct node *n, struct node *a, struct node *b);
struct node *leaf_at_door(struct node *n, int door[2], int8_t cut);
struct node *path_finding(struct node *tree, struct node *start,
                          struct node *end);
void path_to_file(FILE *fp, struct node *n);

/* maze functions */
maze *maze_random(int width, int height);
void maze_svg(struct node *maze, FILE *fp);
void rooms_svg(struct node *maze, FILE *fp, int x_off, int y_off);

#endif

APP = main
CC = gcc
FLAGS = -std=c99 -Wall -Werror

SRC_REP = src
INCLUDE_REP = include
O_REP = obj
BIN_REP = bin

SOURCES  := $(wildcard $(SRC_REP)/*.c)
INCLUDES := $(wildcard $(INCLUDE_REP)/*.h)
OBJECTS  := $(SOURCES:$(SRC_REP)/%.c=$(O_REP)/%.o)

$(BIN_REP)/$(APP): $(OBJECTS)
	$(CC) $(OBJECTS) -o $@


$(OBJECTS): $(O_REP)/%.o : $(SRC_REP)/%.c dirs
	$(CC) $(FLAGS) -c $< -o $@ $(IFLAGS) $(LFLAGS)

dirs: clean
	mkdir $(O_REP) $(BIN_REP)

clean:
	rm -rf $(O_REP) $(BIN_REP)


#include "../include/svg.h"
#include <stdio.h>

#define ZOOM 30

char *svg_color = "#ff0000";

void set_svg_color(char *color) { svg_color = color; }

void svg_header(FILE *f, unsigned int width, unsigned int height) {
  fprintf(f,
          "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" "
          "width=\"%d\" height=\"%d\">\n",
          width * ZOOM, height * ZOOM);
}

void svg_footer(FILE *f) { fprintf(f, "</svg>\n"); }

void svg_line(FILE *f, unsigned int x1, unsigned int y1, unsigned int x2,
              unsigned int y2) {
  if ((x1 == x2) && (y1 == y2)) return;
  fprintf(f,
          "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\" "
          "style=\"stroke:rgb(0,0,0);stroke-width:5\"/>\n",
          x1 * ZOOM, y1 * ZOOM, x2 * ZOOM, y2 * ZOOM);
}

void svg_rect(FILE *f, unsigned int x1, unsigned int y1, unsigned int x2,
              unsigned int y2, char *col) {
  if ((x1 == x2) && (y1 == y2)) return;
  fprintf(
      f,
      "<rect x=\"%d\" y=\"%d\" width=\"%d\" height=\"%d\" fill=\"%s\" "
      "opacity=\"0.4\" style=\"stroke:rgb(255,255,255);stroke-width:0;\"/>\n",
      x1 * ZOOM, y1 * ZOOM, (x2 - x1) * ZOOM, (y2 - y1) * ZOOM, col);
}

void svg_text(FILE *f, unsigned int x, unsigned int y, int id) {
  fprintf(f,
          "<text x=\"%d\" y=\"%d\" font-family=\"Verdana\" "
          "font-size=\"16\">%d</text>",
          x * ZOOM + 3, y * ZOOM + 20, id);
}

#include "../include/maze.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/svg.h"

void cry(char *str) {
  perror(str);
  exit(EXIT_FAILURE);
}

/*
 * init_tree: initialize a tree node
 */
struct node *init_tree(int w, int h) {
  struct node *r = (struct node *)malloc(sizeof(struct node));
  if (r == NULL) cry("malloc");
  r->h = h;
  r->w = w;
  r->way = 0;
  r->left = NULL;
  r->right = NULL;
  return r;
}

/*
 * free_tree: free a tree recursively
 */
void free_tree(struct node *n) {
  if (n) {
    free_tree(n->left);
    free_tree(n->right);
    free(n);
  }
}

/*
 * random_tree: make a random maze recursively until the rooms
 * are of height or width 1
 */
unsigned int random_tree(struct node *n, int id) {
  int r;

  n->id = id;
  if (n->h == 1 || n->w == 1) return id;

  /* Choose between a horizontal or a vertical cut */
  if (n->h == n->w) {
    n->cut = rand() % 2;
  } else if (n->h > n->w) {
    n->cut = 0;
  } else {
    n->cut = 1;
  }

  /* Choosing the position of the door */
  if (n->cut == 0) {
    r = rand() % n->w;
    n->door[0] = r;
    r = (rand() % (n->h - 1)) + 1;
    n->door[1] = r;
    n->left = init_tree(n->w, n->door[1]);
    n->right = init_tree(n->w, n->h - n->door[1]);
  } else {
    r = (rand() % (n->w - 1)) + 1;
    n->door[0] = r;
    r = rand() % n->h;
    n->door[1] = r;
    n->left = init_tree(n->door[0], n->h);
    n->right = init_tree(n->w - n->door[0], n->h);
  }

  /* Do the same on the two rooms just created */
  id++;
  id = random_tree(n->left, id);
  id++;
  id = random_tree(n->right, id);
  return id;
}

/*
 * print_tree: print the tree from left to right
 */
void print_tree(struct node *n, int offset) {
  int i;
  if (n) {
    print_tree(n->left, offset + 7);

    for (i = 0; i < offset; i++) putchar(' ');

    printf("%d ", n->id);
    if (n->left && n->right)
      printf("[%d, %d] ───┤ ", n->w, n->h);
    else if (n->left)
      printf("[%d, %d] ───┘ ", n->w, n->h);
    else if (n->right)
      printf("[%d, %d] ───┐ ", n->w, n->h);
    else
      printf("[%d, %d] ", n->w, n->h);
    printf("    ");

    putchar('\n');
    print_tree(n->right, offset + 7);
  }
}

/*
 * common_node: find the smallest common parent node between a and b in tree n
 */
struct node *common_node(struct node *n, struct node *a, struct node *b) {
  if (a->id == b->id) return n;  // not interesting, ignoring

  if (a->id < n->right->id && b->id < n->right->id)
    return common_node(n->left, a, b);
  else if (a->id >= n->right->id && b->id >= n->right->id)
    return common_node(n->right, a, b);

  return n;
}

/*
 * leaf_at_door: find the leaf (room) that has the door inside
 * of it
 */
struct node *leaf_at_door(struct node *n, int door[2], int8_t cut) {
  if (n->w == 1 || n->h == 1) {
    if (n->way != 1) n->way = 2;
    return n;
  }

  if (cut == 0) {
    if (n->door[1] <= door[1]) {
      door[1] -= n->door[1];
      return leaf_at_door(n->right, door, n->right->cut);
    } else {
      return leaf_at_door(n->left, door, n->left->cut);
    }
  } else {
    if (n->door[0] <= door[0]) {
      door[0] -= n->door[0];
      return leaf_at_door(n->right, door, n->right->cut);
    } else {
      return leaf_at_door(n->left, door, n->left->cut);
    }
  }
}

/*
 * path_finding: find a path from start to end in tree and record it in p.
 */
struct node *path_finding(struct node *tree, struct node *start,
                          struct node *end) {
  if (start->id == end->id) {
    struct node *res = init_tree(start->w, start->h);
    res->id = start->id;
    return res;
  }

  struct node *buf = NULL;
  if (start->id > end->id) {
    buf = start;
    start = end;
    end = buf;
  }

  struct node *parent = common_node(tree, start, end);
  int d1[2], d2[2];
  d1[0] = parent->door[0];
  d1[1] = parent->door[1];
  d2[0] = 0;
  d2[1] = 0;

  if (parent->cut == 0) {
    d2[0] += parent->door[0];
  } else {
    d2[1] += parent->door[1];
  }

  struct node *left_leaf;
  struct node *right_leaf;
  if (!buf) {
    left_leaf = leaf_at_door(parent->left, d1, parent->left->cut);
    right_leaf = leaf_at_door(parent->right, d2, parent->right->cut);
  } else {
    left_leaf = leaf_at_door(parent->right, d2, parent->right->cut);
    right_leaf = leaf_at_door(parent->left, d1, parent->left->cut);
  }

  if (buf) {
    buf = start;
    start = end;
    end = buf;
  }

  struct node *left = path_finding(parent, start, left_leaf);
  struct node *right = path_finding(parent, right_leaf, end);

  struct node *p = left;
  while (p->left) {
    p = p->left;
  }
  p->left = right;
  return left;
}

/*
 * path_to_file: write the path found to a file
 */
void path_to_file(FILE *fp, struct node *n) {
  struct node *c = n;
  while (c->left) {
    fprintf(fp, "%d ->", c->id);
    c = c->left;
  }
  fprintf(fp, "%d\n", c->id);
  return;
}

/*
 * random_point: choose a random leaf in the tree n
 */
struct node *random_point(struct node *n) {
  int r;
  if (n->w > 1 && n->h > 1) {
    r = rand() % 2;
    if (r == 0)
      return random_point(n->left);
    else
      return random_point(n->right);
  }

  n->way = 1;
  return n;
}

void maze_svg(struct node *maze, FILE *fp) {
  svg_line(fp, 0, 0, 0, maze->h);
  svg_line(fp, 0, 0, maze->w, 0);
  svg_line(fp, maze->w, 0, maze->w, maze->h);
  svg_line(fp, 0, maze->h, maze->w, maze->h);
}

void rooms_svg(struct node *maze, FILE *fp, int x_off, int y_off) {
  if (maze->left == NULL && maze->right == NULL) {
    svg_text(fp, x_off, y_off, maze->id);
    if (maze->way == 1) {
      if (maze->h > 1) {
        svg_rect(fp, x_off, y_off, x_off + 1, y_off + maze->h, "red");
      } else {
        svg_rect(fp, x_off, y_off, x_off + maze->w, y_off + 1, "red");
      }
    } else if (maze->way == 2) {
      if (maze->h > 1) {
        svg_rect(fp, x_off, y_off, x_off + 1, y_off + maze->h, "blue");
      } else {
        svg_rect(fp, x_off, y_off, x_off + maze->w, y_off + 1, "blue");
      }
    }
    return;
  }

  int x1 = 0;
  int y1 = 0;
  int x2 = maze->door[0];
  int y2 = maze->door[1];

  if (maze->cut == 1) x1 += maze->door[0];
  if (maze->cut == 0) y1 += maze->door[1];

  svg_line(fp, x1 + x_off, y1 + y_off, x2 + x_off, y2 + y_off);

  x1 = x2;
  y1 = y2;

  if (maze->cut == 1) y1++;
  if (maze->cut == 0) x1++;

  x2 = maze->cut == 0 ? maze->w : maze->door[0];
  y2 = maze->cut == 1 ? maze->h : maze->door[1];

  svg_line(fp, x1 + x_off, y1 + y_off, x2 + x_off, y2 + y_off);

  if (maze->cut == 1) {
    rooms_svg(maze->left, fp, x_off, y_off);
    rooms_svg(maze->right, fp, maze->door[0] + x_off, y_off);
  } else {
    rooms_svg(maze->left, fp, x_off, y_off);
    rooms_svg(maze->right, fp, x_off, maze->door[1] + y_off);
  }
}

maze *maze_random(int width, int height) { return NULL; }


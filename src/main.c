#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "../include/maze.h"
#include "../include/svg.h"

int main(int argc, char** argv) {
  unsigned int w, h;

  /* init: making sure parameters are compliant with the program */
  if (argc < 3 || (w = atoi(argv[1])) <= 0 || (h = atoi(argv[2])) <= 0) {
    fprintf(stderr, "Usage: %s <width> <height>\n", *argv);
    return 1;
  }

  /* re-seed rand */
  srand(time(NULL));

  char* file = "maze.svg";
  char* output = "path.output";
  struct node *tree = init_tree(w, h), *a, *b;

  /* maze creation */
  random_tree(tree, 1);

  /* choosing two random points, a position and an exit */
  a = random_point(tree);
  b = random_point(tree);

  /* initializing the path */
  struct node* path = path_finding(tree, a, b);

  /* svg saving */
  FILE* fp = fopen(file, "w");
  svg_header(fp, tree->w, tree->h);

  maze_svg(tree, fp);
  rooms_svg(tree, fp, 0, 0);

  svg_footer(fp);
  fclose(fp);
  printf("Everything has been written in: %s\n", file);

  /* writing the path to an output file*/
  fp = fopen(output, "w");
  path_to_file(fp, path);
  fclose(fp);

  print_tree(path, 0);

  free_tree(path);
  free_tree(tree);
  return 0;
}

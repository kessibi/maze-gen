# Maze Generation and Path Finding

## How to use it

- run `make` to compile the sources;
- run `./bin/main <x> <y>`;

The files are saved in `maze.svg` and the path in `path.output`.
